#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h" 


void delaybyms (unsigned int); // prototype

void delaybyms(unsigned int j) {
unsigned int k,l;
for(k=0;k<j;k++)
for(l=0;l<1427;l++);
}

char get_key (void) {
RCC->AHBENR |= 1 << 19; // Enable GPIOC clock
GPIOC->MODER |= 0x1500; // configure PC[6:4] for output and PC[3:0] for input

while (1) {
GPIOC->ODR = 0x60; // prepare to scan the row controlled by PC4

if (!(GPIOC->IDR & 0x01)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x01))
return 0x2A; // return ASCII code of *
}

if (!(GPIOC->IDR & 0x02)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x02))
return 0x37; // return ASCII code of 7
}

if (!(GPIOC->IDR & 0x04)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x04))
return 0x34; // return ASCII code of 4
}

if (!(GPIOC->IDR & 0x08)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x08))
return 0x31; // return ASCII code of 1
}

GPIOC->ODR = 0x50; // set PC5 to low to scan second row

if (!(GPIOC->IDR & 0x01)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x01))
return 0x30; // return ASCII code of 0
}

if (!(GPIOC->IDR & 0x02)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x02))
return 0x38; // return ASCII code of 8
}

if (!(GPIOC->IDR & 0x04)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x04))
return 0x35; // return ASCII code of 5
}

if (!(GPIOC->IDR & 0x08)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x08))
return 0x32; // return ASCII code of 2
}

GPIOC->ODR = 0x30; // set PC6 to low to scan the third row

if (!(GPIOC->IDR & 0x01)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x01))
return 0x23; // return ASCII code of #
}

if (!(GPIOC->IDR & 0x02)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x02))
return 0x39; // return ASCII code of 9
}

if (!(GPIOC->IDR & 0x04)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x04))
return 0x36; // return ASCII code of 6
}

if (!(GPIOC->IDR & 0x08)) {
delaybyms (10);
if (!(GPIOC->IDR & 0x08))
return 0x33; // return ASCII code of 3
}

}
}