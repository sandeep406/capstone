/* ************************************************* * 
Function: SPI_Read () 
Description : Reads one byte from the reg register 
*********************************** ************** */ 
void RegWrite(int mode)      
{
    uint32_t val;
    uint32_t reg;
    if(mode != CMD_INTERACTIVE) return;
    reg = 0;
    val = 0;
    fetch_uint32_arg(&reg);
    fetch_uint32_arg(&val);
    SPI3_readWriteReg(RF_WRITE_REG+reg, val);
    printf("Register 0x%02x : 0x%02x\n",
            (unsigned int)reg,    SPI3_readReg(reg));       
}
ADD_CMD("LoadReg",RegWrite,"   Loadregister value ");


/* ****************************** ******************** 
Function: SPI3_readWriteReg () Description 
: Write data value to reg register 
***************** *****u******************* */ 

void CmdReg_Val(int mode)      
{
    if(mode != CMD_INTERACTIVE) return;
    SPI3_readWriteReg(RF_WRITE_REG + STATUS, 0x87); 
}

ADD_CMD("TestVal",CmdReg_Val,"  register value ");


/* ************************************************* * 
Function: SPI_Read_Buf () 
Description : Read bytes from the reg register, usually used to read the receive channel 
   data or receive / send address 
******************* ***** ************************* */ 
void GetRegVal(int mode)      
{
    uint32_t reg;
    if(mode != CMD_INTERACTIVE) return;
    reg = 0;
    fetch_uint32_arg(&reg);
    printf("Register 0x%02x : 0x%02x\n",
            (unsigned int)reg,    SPI3_readReg(reg));
        
}

ADD_CMD("TestRG",GetRegVal,"  Register Value stored");

/* ************************************************* * 
function: SPI_Write_Buf () 
description: the data is written to the cache pBuf nRF24L01, usually used to write send 
   radio channel data or receive / transmit address 
***************** ***** *************************** */ 
unsigned char SPI3_writeBuf(unsigned char reg, unsigned char *pBuf,
		unsigned char bytes) {
	unsigned char status, i;
	CSN_L();       // CSN set low, start
	status = SPI3_readWrite(reg);// Select register to write to and return the status word
	for (i = 0; i < bytes; i++) // then write all byte in buffer(*pBuf)
		SPI3_readWrite(*pBuf++);
	CSN_H(); // CSN is pulled high, ending data transfer 
	return (status);// return to status register 
}
